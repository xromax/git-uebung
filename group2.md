Exercise:

Explain the following commands with one sentence:

```text
$ git checkout master	Switches to master branch
$ git branch feature1	Creates feature1 branch
$ git checkout -b work	Creates work branch and switches to work
$ git checkout work		Switches to work branch
$ git branch			Erstellt ausgehend vom aktuellen Stand des Repos einen neuen Branch.
$ git branch -v			shows commit subject line and sha1 of each HEAD.
$ git branch -a			Option -a shows both local and remote branches. active branch is marked with asterix.
$ git branch --merged	Shows branches merged in the past
$ git branch --no-merged	Shows branches which are not merged yet
$ git branch -d work	Deletes branch work, if it's merged in its upstream branch
$ git branch -D work	Deletes branch work, even if it's not merged (-d --force)
```

Example:

    $ git status         Shows the current state of the repository

Add you answers to the file readme.md!
